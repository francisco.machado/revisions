function counter () {
    var c = 0;

    return {
        decrement: function() {
            c--;
        },
        increment: function() {
            c++;
        },
        getValue: function() {
            return c;
        }
    };
}

function counterIncrementer(counter, times) {
    while (times) {
        counter.increment();
        times--;
    }
}

function executor(fn) {
    fn();
}

var test = counter();

counterIncrementer(test, 5);
executor(test.decrement);

console.log(test.getValue());