package org.academiadecodigo.races;

public interface Positionable {

    int getPosition();

    char getRepresentation();
}
