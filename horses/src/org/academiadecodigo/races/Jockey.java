package org.academiadecodigo.races;

import org.academiadecodigo.races.horses.Horse;

public class Jockey implements Positionable {

    private String name;
    private Horse horse;

    public Jockey(String name, Horse horse) {
        this.name = name;
        this.horse = horse;
    }

    public void run() {
        horse.run();
    }

    public int getPosition() {
        return horse.getCol();
    }

    @Override
    public char getRepresentation() {
        return horse.getRepresentation();
    }
}
