package org.academiadecodigo.races.horses;

public class LighteningHorse extends Horse {

    private static final int SPEED = 3;

    public LighteningHorse(String name) {
        super(name);
    }

    @Override
    public void run() {
        for (int i = 0; i < SPEED; i++) {
            super.run();
        }
    }

    @Override
    public char getRepresentation() {
        return 'L';
    }
}
