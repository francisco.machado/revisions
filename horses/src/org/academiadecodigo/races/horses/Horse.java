package org.academiadecodigo.races.horses;

public abstract class Horse {

    private String name;
    private int col;

    public Horse(String name) {
        this.name = name;
        col = 0;
    }

    public void run() {
        col++;
    }

    public String getName() {
        return name;
    }

    public int getCol() {
        return col;
    }

    public abstract char getRepresentation();
}
