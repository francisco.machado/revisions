package org.academiadecodigo.races.horses;

public class DrunkHorse extends Horse{

    private final int alchool = 3;

    public DrunkHorse(String name) {
        super(name);
    }

    @Override
    public void run() {

        int rand = (int) (Math.random() * alchool);

        if (rand == 0) {
            super.run();
        }
    }

    @Override
    public char getRepresentation() {
        return 'D';
    }
}
