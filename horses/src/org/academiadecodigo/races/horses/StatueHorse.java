package org.academiadecodigo.races.horses;

public class StatueHorse extends Horse {

    public StatueHorse(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println("Just a statue");
    }

    @Override
    public char getRepresentation() {
        return 'S';
    }
}
