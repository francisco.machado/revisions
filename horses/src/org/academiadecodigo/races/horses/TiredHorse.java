package org.academiadecodigo.races.horses;

public class TiredHorse extends Horse {

    private static final int MOVES_BEFORE_TIRED = 3;
    private int moves;

    public TiredHorse(String name) {
        super(name);
        moves = 0;
    }

    @Override
    public void run() {

        if (moves == MOVES_BEFORE_TIRED) {
            moves = 0;
            return;
        }

        super.run();
        moves++;
    }

    @Override
    public char getRepresentation() {
        return 'T';
    }
}
