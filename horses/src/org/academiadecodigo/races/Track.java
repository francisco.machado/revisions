package org.academiadecodigo.races;

import org.academiadecodigo.races.graphics.Field;
import org.academiadecodigo.races.graphics.LanternaField;

public class Track {

    private final int delay;
    private int length;
    private Jockey[] jockeys;
    private Field field;

    public Track(int length, Jockey[] jockeys, int delay) {
        this.length = length;
        this.jockeys = jockeys;
        this.delay = delay;
    }

    public void init(Field field) {
        this.field = field;
        field.init(length, jockeys.length);
    }

    public void start() throws InterruptedException {

        boolean over = false;

        while (!over) {

            field.draw(jockeys);

            Thread.sleep(delay);

            for (Jockey j : jockeys) {
                j.run();

                if (j.getPosition() >= length) {
                    over = true;
                }
            }
        }
    }
}
