package org.academiadecodigo.races.graphics;

import org.academiadecodigo.races.Positionable;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;

import java.util.LinkedList;
import java.util.List;

public class GfxField implements Field {

    private static final int PADDING = 10;
    private static final int CELL_WIDTH = 10;
    private static final int CELL_HEIGHT = 15;
    private Rectangle background;
    private List<Text> drawings;

    @Override
    public void init(int cols, int rows) {

        background = new Rectangle(PADDING, PADDING, cols * CELL_WIDTH, rows * CELL_HEIGHT);
        background.draw();

        drawings = new LinkedList<>();
    }

    @Override
    public void draw(Positionable[] positionables) {

        clear();

        for (int i = 0; i < positionables.length; i++) {
            Positionable p = positionables[i];


            Text text = new Text(colToX(p.getPosition()), rowToY(i), p.getRepresentation() + "");
            text.draw();
            drawings.add(text);
        }

    }

    private void clear() {

        for (Text t : drawings) {
            t.delete();
        }

        drawings.clear();
    }

    private int colToX(int col) {
        return col * CELL_WIDTH + PADDING;
    }

    private int rowToY(int row) {
        return row * CELL_HEIGHT + PADDING;
    }
}
