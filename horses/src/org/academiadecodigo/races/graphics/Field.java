package org.academiadecodigo.races.graphics;

import org.academiadecodigo.races.Positionable;

public interface Field {

    void init(int cols, int rows);
    void draw(Positionable[] positionables);
}
