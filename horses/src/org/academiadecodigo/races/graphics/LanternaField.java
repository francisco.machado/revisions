package org.academiadecodigo.races.graphics;

import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.ScreenWriter;
import com.googlecode.lanterna.terminal.Terminal;
import org.academiadecodigo.races.Positionable;

public class LanternaField implements Field{

    // Used to write to screen
    private Screen screen;

    // Screen wrapper that preserves default options
    private ScreenWriter screenWriter;

    //This class is not supposed to be instantiated

    public void init(int cols, int rows) {

        // Create the GUI
        screen = TerminalFacade.createScreen();

        // Set field size
        screen.setCursorPosition(null);
        screen.getTerminal().getTerminalSize().setColumns(cols);
        screen.getTerminal().getTerminalSize().setRows(rows);

        // Default screen writing options
        screenWriter = new ScreenWriter(screen);
        screenWriter.setBackgroundColor(Terminal.Color.BLUE);
        screenWriter.setForegroundColor(Terminal.Color.WHITE);

        screen.startScreen();

    }

    public void draw(Positionable[] positionables) {

        screen.clear();

        for (int i = 0; i < positionables.length; i++) {

            screenWriter.drawString(positionables[i].getPosition(), i, positionables[i].getRepresentation() + "");
        }

        screen.refresh();

    }
}
