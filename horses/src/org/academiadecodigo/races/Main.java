package org.academiadecodigo.races;

import org.academiadecodigo.races.graphics.GfxField;
import org.academiadecodigo.races.graphics.LanternaField;
import org.academiadecodigo.races.horses.DrunkHorse;
import org.academiadecodigo.races.horses.LighteningHorse;
import org.academiadecodigo.races.horses.TiredHorse;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        Jockey[] jockeys = {
                new Jockey("Rudy", new LighteningHorse("Scotch")),
                new Jockey("Pantoninho", new TiredHorse("Ze Bufas")),
                new Jockey("Catarina", new DrunkHorse("Pantoninho"))
        };

        Track track = new Track(100, jockeys, 200);
        track.init(new LanternaField());
        track.start();

        System.out.println("over");

    }
}
