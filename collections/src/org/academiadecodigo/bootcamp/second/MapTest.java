package org.academiadecodigo.bootcamp.second;

import org.academiadecodigo.bootcamp.Popstar;
import org.academiadecodigo.bootcamp.Work;

public class MapTest {

    public static void main(String[] args) {

        Popstar adriano = new Popstar("Adriano", "Ponho o CARRO tiro o CARRO!");
        Popstar duarte = new Popstar("Duarte", "A mulher gorda a mim não me convem!");
        Popstar humberto = new Popstar("Tio Humberto", "Hold my beer!");

        MapClass turmaWithMap = new MapClass();

        turmaWithMap.add(adriano);
        turmaWithMap.add(duarte);
        turmaWithMap.add(humberto);

        turmaWithMap.choir();

        Work invaders = new Work("Invaders");
        Work jammers = new Work("Beer Jammers");
        Work hotel = new Work("Hotel");
        Work piggy = new Work("Piggy Bank");

        turmaWithMap.addWork(humberto, invaders);
        turmaWithMap.addWork(adriano, invaders);
        turmaWithMap.addWork(duarte, jammers);
        turmaWithMap.addWork(duarte, hotel);
        turmaWithMap.addWork(duarte, piggy);
        turmaWithMap.showWorks(duarte);
    }
}
