package org.academiadecodigo.bootcamp.second;

import org.academiadecodigo.bootcamp.Popstar;
import org.academiadecodigo.bootcamp.Work;

import java.util.*;

public class MapClass {

    // if they suggest key to be the name (String) come to the conclusion that I cannot make a String sing...
    // remember that if we override Popstar equals we need to override hashCode as well
    private Map<Popstar, List<Work>> popstars = new HashMap<>();

    public void add(Popstar popstar) {
        popstars.put(popstar, new LinkedList<>());
    }

    public void addWork(Popstar popstar, Work work) {
        if (!popstars.containsKey(popstar)) {
            add(popstar);
        }

        popstars.get(popstar).add(work);
    }

    public void showWorks(Popstar popstar) {
        List<Work> works = popstars.get(popstar);

        System.out.println("Works from " + popstar.getName() +":");
        for (Work work : works) {
            System.out.println(work.getTitle());
        }
    }

    public void choir() {
        Iterator<Popstar> iterator = popstars.keySet().iterator();

        while (iterator.hasNext()) {
            Popstar popstar = iterator.next();

            System.out.println(popstar.sing());
        }
    }
}
