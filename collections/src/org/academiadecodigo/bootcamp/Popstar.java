package org.academiadecodigo.bootcamp;

import java.util.Objects;

public class Popstar {

    private String name;
    private String song;

    public Popstar(String name, String song) {
        this.name = name;
        this.song = song;
    }

    public String sing() {
        return name + ": " + song;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Popstar popstar = (Popstar) o;
        return Objects.equals(name, popstar.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
