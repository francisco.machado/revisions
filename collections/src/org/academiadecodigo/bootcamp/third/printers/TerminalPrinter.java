package org.academiadecodigo.bootcamp.third.printers;

import org.academiadecodigo.bootcamp.third.Printer;

public class TerminalPrinter implements Printer {
    @Override
    public void print(String text) {
        System.out.println(text);
    }
}
