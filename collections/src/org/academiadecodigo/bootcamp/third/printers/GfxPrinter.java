package org.academiadecodigo.bootcamp.third.printers;

import org.academiadecodigo.bootcamp.third.Printer;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;

public class GfxPrinter implements Printer {

    private static final int PADDING = 10;
    private static final int WIDTH = 350;
    private static final int HEIGHT = 100;
    private Rectangle background;
    private Text text;

    public GfxPrinter() {
        background = new Rectangle(PADDING, PADDING, WIDTH, HEIGHT);
        text = new Text(10, 10, "A");
        background.draw();
        text.translate(0, HEIGHT / 2  + PADDING - text.getWidth() / 2 - text.getY());
    }

    @Override
    public void print(String text) {
        this.text.delete();
        this.text.setText(text);

        this.text.translate(
                WIDTH / 2  + PADDING - this.text.getWidth() / 2 - this.text.getX(),
                0
        );

        this.text.draw();
    }
}
