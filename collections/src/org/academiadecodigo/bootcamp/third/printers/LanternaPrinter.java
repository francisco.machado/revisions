package org.academiadecodigo.bootcamp.third.printers;

import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.ScreenWriter;
import com.googlecode.lanterna.terminal.Terminal;
import org.academiadecodigo.bootcamp.third.Printer;

public class LanternaPrinter implements Printer {

    private static final int PADDING = 1;
    private static final int WIDTH = 60;
    private Screen screen;
    private ScreenWriter screenWriter;

    public LanternaPrinter() {
        screen = TerminalFacade.createScreen();
        screen.setCursorPosition(null);
        screen.getTerminal().getTerminalSize().setColumns(PADDING * 2 + WIDTH);
        screen.getTerminal().getTerminalSize().setRows(PADDING * 2 + 1);

        // Default screen writing options
        screenWriter = new ScreenWriter(screen);
        screenWriter.setBackgroundColor(Terminal.Color.BLUE);
        screenWriter.setForegroundColor(Terminal.Color.WHITE);

        screen.startScreen();
    }

    @Override
    public void print(String text) {

        screen.clear();

        screenWriter.drawString(
                WIDTH / 2 + PADDING - text.length() / 2,
                PADDING,
                text
        );

        screen.refresh();
    }
}
