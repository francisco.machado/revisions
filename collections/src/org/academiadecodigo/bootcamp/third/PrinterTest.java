package org.academiadecodigo.bootcamp.third;

import org.academiadecodigo.bootcamp.Popstar;
import org.academiadecodigo.bootcamp.Work;
import org.academiadecodigo.bootcamp.third.printers.GfxPrinter;
import org.academiadecodigo.bootcamp.third.printers.LanternaPrinter;
import org.academiadecodigo.bootcamp.third.printers.TerminalPrinter;

public class PrinterTest {

    public static void main(String[] args) {

        Popstar adriano = new Popstar("Adriano", "Ponho o CARRO tiro o CARRO!");
        Popstar duarte = new Popstar("Duarte", "A mulher gorda a mim não me convem!");
        Popstar humberto = new Popstar("Tio Humberto", "Hold my beer!");

        PrintableClass turmaWithMap = new PrintableClass();
        Printer terminal = new TerminalPrinter();
        Printer lanterna = new LanternaPrinter();
        Printer gfx = new GfxPrinter();

        turmaWithMap.add(adriano);
        turmaWithMap.add(duarte);
        turmaWithMap.add(humberto);

        Work invaders = new Work("Invaders");
        Work windjammers = new Work("Beer Jammers");
        Work hotel = new Work("Hotel");
        Work piggy = new Work("Piggy Bank");

        turmaWithMap.addWork(humberto, invaders);
        turmaWithMap.addWork(adriano, invaders);
        turmaWithMap.addWork(duarte, windjammers);
        turmaWithMap.addWork(duarte, hotel);
        turmaWithMap.addWork(duarte, piggy);

        turmaWithMap.setPrinter(terminal);
        turmaWithMap.choir();
        turmaWithMap.showWorks(humberto);

        turmaWithMap.setPrinter(lanterna);
        turmaWithMap.choir();
        turmaWithMap.showWorks(adriano);

        turmaWithMap.setPrinter(gfx);
        turmaWithMap.choir();
        turmaWithMap.showWorks(duarte);

    }
}
