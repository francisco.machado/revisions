package org.academiadecodigo.bootcamp.third;

public interface Printer {
    void print(String text);
}
