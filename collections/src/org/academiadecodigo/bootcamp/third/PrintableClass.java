package org.academiadecodigo.bootcamp.third;

import org.academiadecodigo.bootcamp.Popstar;
import org.academiadecodigo.bootcamp.Work;

import java.util.*;

public class PrintableClass {

    private Map<Popstar, List<Work>> popstars = new HashMap<>();
    private Printer printer;

    public void add(Popstar popstar) {
        if (popstars.containsKey(popstar)) {
            return;
        }

        popstars.put(popstar, new LinkedList<>());
    }

    public void addWork(Popstar popstar, Work work) {
        if (!popstars.containsKey(popstar)) {
            add(popstar);
        }

        popstars.get(popstar).add(work);
    }

    public void showWorks(Popstar popstar) {
        List<Work> works = popstars.get(popstar);
        String worksText = popstar.getName() + ": | ";

        for (Work work : works) {
            worksText += work.getTitle() + " | ";
        }

        printer.print(worksText);
    }

    public void choir() {
        Iterator<Popstar> iterator = popstars.keySet().iterator();

        while (iterator.hasNext()) {
            Popstar popstar = iterator.next();
            printer.print(popstar.sing());

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setPrinter(Printer printer) {
        this.printer = printer;
    }
}
