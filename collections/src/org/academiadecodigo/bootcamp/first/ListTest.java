package org.academiadecodigo.bootcamp.first;

import org.academiadecodigo.bootcamp.Popstar;

public class ListTest {

    public static void main(String[] args) {

        Popstar adriano = new Popstar("Adriano", "Ponho o CARRO tiro o CARRO!");
        Popstar duarte = new Popstar("Duarte", "A mulher gorda a mim não me convem!");
        Popstar humberto = new Popstar("Tio Humberto", "Hold my beer!");


        ListClass turmaWithList = new ListClass();

        turmaWithList.add(adriano);
        turmaWithList.add(duarte);
        turmaWithList.add(humberto);

        turmaWithList.choir();

    }
}
