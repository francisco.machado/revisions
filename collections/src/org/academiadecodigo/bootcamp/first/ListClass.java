package org.academiadecodigo.bootcamp.first;

import org.academiadecodigo.bootcamp.Popstar;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ListClass {

    // insert the same object twice and make example with a set a override Popstar equals
    private List<Popstar> popstars = new LinkedList<>();

    public void add(Popstar popstar) {
        popstars.add(popstar);
    }

    public void choir() {
        Iterator<Popstar> iterator = popstars.iterator();

        while (iterator.hasNext()) {
            Popstar popstar = iterator.next();
            System.out.println(popstar.sing());
        }
    }
}
