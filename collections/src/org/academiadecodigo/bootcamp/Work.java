package org.academiadecodigo.bootcamp;

public class Work {

    private String title;

    public Work(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
