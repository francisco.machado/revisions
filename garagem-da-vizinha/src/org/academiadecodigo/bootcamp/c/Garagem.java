package org.academiadecodigo.bootcamp.c;

public class Garagem {

    private Vehicle[] spots;
    private int freeSpots;

    public Garagem(int capacity) {
        spots = new Vehicle[capacity];
        freeSpots = spots.length;
    }

    public boolean park(Vehicle vehicle) {
        for (int i = 0; i < spots.length; i++) {
            if (spots[i] != null) {
                continue;
            }

            vehicle.park(i);

            spots[i] = vehicle;
            freeSpots--;
            return true;
        }

        return false;
    }

    public boolean unpark(Vehicle vehicle) {
        if (!vehicle.isParked()) {
            return false;
        }

        int spot = vehicle.getParkingSpot();
        return unpark(spot);
    }

    public boolean unpark(int spotNr) {
        if (spotNr > spots.length - 1) {
            return false;
        }

        if (spots[spotNr] == null) {
            return false;
        }

        spots[spotNr].unpark();

        spots[spotNr] = null;
        freeSpots++;
        return true;
    }


    public int getCapacity() {
        return spots.length;
    }

    public int getFreeSpots() {
        return freeSpots;
    }

    @Override
    public String toString() {
        String garageRepresentation = "";

        for (int i = 0; i < spots.length; i++) {
            garageRepresentation += "|" + (spots[i] != null ? spots[i].getBrand().substring(0, 3) : " " + i + " ");
        }

        garageRepresentation += "|";
        return garageRepresentation;
    }
}
