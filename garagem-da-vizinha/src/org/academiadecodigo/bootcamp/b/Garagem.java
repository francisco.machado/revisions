package org.academiadecodigo.bootcamp.b;

public class Garagem {

    private boolean[] spots;
    private int freeSpots;

    public Garagem(int capacity) {
        spots = new boolean[capacity];
        freeSpots = spots.length;
    }

    public boolean park() {
        for (int i = 0; i < spots.length; i++) {
            if (spots[i]) {
                continue;
            }

            spots[i] = true;
            freeSpots--;
            return true;
        }

        return false;
    }

    public boolean unpark(int spot) {
        if (spot > spots.length - 1) {
            return false;
        }

        if (!spots[spot]) {
            return false;
        }

        spots[spot] = false;
        freeSpots++;
        return true;
    }


    public int getCapacity() {
        return spots.length;
    }

    public int getFreeSpots() {
        return freeSpots;
    }

    @Override
    public String toString() {
        String garageRepresentation = "";

        for (int i = 0; i < spots.length; i++) {
            garageRepresentation += "|" + (spots[i] ? "X" : i);
        }

        garageRepresentation += "|";
        return garageRepresentation;
    }
}
