package org.academiadecodigo.bootcamp.a;

public class GaragemTester {

    public static void main(String[] args) {

        Garagem g = new Garagem(2);

        System.out.println("Should be empty after creating");
        System.out.println(g);
        System.out.println("----------");

        System.out.println("Should not be able to park in spot bigger than capacity");
        System.out.println("parked? " + g.park(10));
        System.out.println(g);
        System.out.println("----------");

        System.out.println("Should be able to park in empty spot");
        System.out.println("parked? " + g.park(0));
        System.out.println(g);
        System.out.println("----------");

        System.out.println("Should be able to park in empty spot");
        System.out.println("parked? " + g.park(1));
        System.out.println(g);
        System.out.println("----------");

        System.out.println("Should not be able to park in busy spot");
        System.out.println("parked? " + g.park(1));
        System.out.println(g);
        System.out.println("----------");

        System.out.println("Should be able to unpark a busy spot");
        System.out.println("unparked? " + g.unpark(1));
        System.out.println(g);
        System.out.println("----------");

        System.out.println("Should be able to unpark a busy spot");
        System.out.println("unparked? " + g.unpark(0));
        System.out.println(g);
        System.out.println("----------");

        System.out.println("Should not be able to unpark an empty spot");
        System.out.println("unparked? " + g.unpark(0));
        System.out.println(g);
        System.out.println("----------");
    }
}
