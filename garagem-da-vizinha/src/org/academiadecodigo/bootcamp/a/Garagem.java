package org.academiadecodigo.bootcamp.a;

public class Garagem {

    private boolean[] spots;
    private int freeSpots;

    public Garagem(int capacity) {
        spots = new boolean[capacity];
        freeSpots = spots.length;
    }

    public boolean park(int spotNr) {
        if (spotNr > spots.length - 1) {
            return false;
        }

        if (spots[spotNr]) {
            return false;
        }

        spots[spotNr] = true;
        freeSpots--;
        return true;
    }

    public boolean unpark(int spotNr) {
        if (spotNr > spots.length - 1) {
            return false;
        }

        if (!spots[spotNr]) {
            return false;
        }

        spots[spotNr] = false;
        freeSpots++;
        return true;
    }


    public int getCapacity() {
        return spots.length;
    }

    public int getFreeSpots() {
        return freeSpots;
    }

    @Override
    public String toString() {
        String garageRepresentation = "";

        for (int i = 0; i < spots.length; i++) {
            garageRepresentation += "|" + (spots[i] ? "X" : i);
        }

        garageRepresentation += "|";
        return garageRepresentation;
    }
}
