package org.academiadecodigo.bootcamp.d;

public class GaragemDosRicos extends Garagem {

    private static String[] richBrands = {"Bentley", "Ferrari", "Maseratti", "Lamborghini"};

    public GaragemDosRicos(int capacity) {
        super(capacity);
    }

    @Override
    public boolean park(Vehicle vehicle) {
        if (!isRichBrand(vehicle)) {
            return false;
        }

        return super.park(vehicle);
    }

    private boolean isRichBrand(Vehicle vehicle) {
        for (String brand : richBrands) {
            if (vehicle.getBrand().equals(brand)) {
                return true;
            }
        }

        return false;
    }
}
