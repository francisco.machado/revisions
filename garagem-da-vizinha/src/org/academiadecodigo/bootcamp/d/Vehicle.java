package org.academiadecodigo.bootcamp.d;

public class Vehicle {

    private String brand;
    private int parkingSpot;

    public Vehicle(String brand) {
        this.brand = brand;
    }

    public void park(int spot) {
        this.parkingSpot = spot;
    }

    public void unpark() {
        this.parkingSpot = -1;
    }

    public boolean isParked() {
        return parkingSpot != -1;
    }

    public int getParkingSpot() {
        return parkingSpot;
    }

    public String getBrand() {
        return brand;
    }
}
