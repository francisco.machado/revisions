package org.academiadecodigo.bootcamp.library;

import java.util.LinkedList;
import java.util.List;

public class Library {

    private List<Book> books;
    private View view;

    public Library() {
        books = new LinkedList<>();
        view = new View();
    }

    public void init() {

        boolean exit = false;

        while (!exit) {
            int option = view.showMenu();

            switch (option) {
                case 1:
                    view.listBooks(books);
                    break;

                case 2:
                    books.add(view.createBook());
                    break;

                default:
                    exit = true;
                    break;
            }
        }
    }
}
