package org.academiadecodigo.bootcamp.library;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

import java.util.List;

public class View {

    private Prompt prompt;

    public View() {
        prompt = new Prompt(System.in, System.out);
    }

    public int showMenu() {

        MenuInputScanner menu = new MenuInputScanner(new String[] {
                "List Books",
                "Add Book",
                "Exit"
        });

        menu.setMessage("What would you like to do?");

        return prompt.getUserInput(menu);
    }

    public void listBooks(List<Book> books) {

        if (books.isEmpty()) {
            System.out.println("There are no books here");
            return;
        }

        System.out.print("Books: ");

        for (Book book : books) {
            System.out.print(book.getTitle() + " | ");
        }

    }

    public Book createBook() {

        StringInputScanner title = new StringInputScanner();
        title.setMessage("Title: ");

        StringInputScanner author = new StringInputScanner();
        author.setMessage("Author: ");

        IntegerInputScanner year = new IntegerInputScanner();
        year.setMessage("Year: ");

        Book book = new Book();

        book.setTitle(prompt.getUserInput(title));
        book.setAuthor(prompt.getUserInput(author));
        book.setYear(prompt.getUserInput(year));

        return book;
    }
}
