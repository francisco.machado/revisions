package org.academiadecodigo.bootcamp.copy;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileManager {

    public static void copy(InputStream from, OutputStream to) throws IOException {
        byte[] data = new byte[1024];
        int count;

        while ((count = from.read(data, 0, data.length)) != -1) {
            to.write(data, 0, count);
        }
    }

    public static String getExtension(String filePath) {
        int point = filePath.lastIndexOf('.');

        if (point == -1) {
            return filePath;
        }

        return filePath.substring(point);
    }

    public static void close(Closeable closeable) {
        if (closeable == null) {
            return;
        }

        try {
            closeable.close();
        } catch (IOException e) {
            System.err.println("Error closing stream: " + e.getMessage());
        }
    }
}
