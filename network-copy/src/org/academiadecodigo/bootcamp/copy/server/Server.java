package org.academiadecodigo.bootcamp.copy.server;

import org.academiadecodigo.bootcamp.copy.FileManager;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private ServerSocket socket;

    public Server(int port) throws IOException {
        socket = new ServerSocket(port);
    }

    public void start() {

        Socket client = null;
        BufferedOutputStream writer = null;

        while (!socket.isClosed()) {

            try {
                client = socket.accept();

                DataInputStream reader = new DataInputStream(client.getInputStream());
                byte[] data = new byte[1024];

                int size = reader.read(data);
                String filePath = new String(data, 0, size);

                writer = new BufferedOutputStream(new FileOutputStream(filePath));

                FileManager.copy(reader, writer);

            } catch (IOException e) {
                System.err.println("Error handling client: " + e.getMessage());

            } finally {
                FileManager.close(client);
                FileManager.close(writer);
            }
        }
    }

    public static void main(String[] args) {

        if (args.length != 1) {
            System.out.println("Usage: <port>");
            return;
        }

        try {
            Server server = new Server(Integer.valueOf(args[0]));
            server.start();

        } catch (IOException e) {
            System.err.println("Error creating Server Socket: " + e.getMessage());

        } catch (NumberFormatException e) {
            System.err.println("Error: port must be a valid number");
        }
    }
}
