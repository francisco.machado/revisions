package org.academiadecodigo.bootcamp.copy.client;

import org.academiadecodigo.bootcamp.copy.FileManager;

import java.io.*;
import java.net.Socket;

public class Client {

    private Socket socket;

    public Client(String host, int port) throws IOException {
        socket = new Socket(host, port);
    }

    public void send(String filePath) throws FileNotFoundException {

        BufferedInputStream reader = null;
        BufferedOutputStream writer = null;

        try {
            reader = new BufferedInputStream(new FileInputStream(filePath));
            writer = new BufferedOutputStream(socket.getOutputStream());

            writer.write(filePath.getBytes());
            writer.flush();

            FileManager.copy(reader, writer);

        } catch (FileNotFoundException e) {
            throw e;

        } catch (IOException e) {
            System.err.println("Error transferring data: " + e.getMessage());

        } finally {
            FileManager.close(reader);
            FileManager.close(writer);
        }
    }

    public static void main(String[] args) {

        if (args.length != 3) {
            System.out.println("Usage: <server_ip> <port> <file_path>");
            return;
        }

        try {
            Client client = new Client(args[0], Integer.valueOf(args[1]));
            client.send(args[2]);

        } catch (FileNotFoundException e) {
            System.err.println("Error: file not found");

        } catch (IOException e) {
            System.err.println("Error connecting: " + e.getMessage());
        }

    }
}
