package org.academiadecodigo.bootcamp.udp_ping.client;

import org.academiadecodigo.bootcamp.udp_ping.Messages;

import java.io.IOException;
import java.net.*;

public class Client {

    private DatagramSocket socket;

    public Client() throws SocketException {
        socket = new DatagramSocket();
    }

    public void ping(String host, int port, int times) throws UnknownHostException {
        InetAddress address = InetAddress.getByName(host);

        long totalTime = 0;

        for (int i = 0; i < times; i++) {
            totalTime += sendRequest(address, port);
        }

        System.out.println("Average RTT: " + (totalTime / times));
        socket.close();
    }

    private long sendRequest(InetAddress address, int port) {
        long delta = 0;

        try {

            byte[] message = Messages.PING.getBytes();
            DatagramPacket toSend = new DatagramPacket(
                    message,
                    message.length,
                    address,
                    port
            );

            byte[] data = new byte[512];
            DatagramPacket received = new DatagramPacket(data, data.length);

            long timeLeft = System.currentTimeMillis();
            socket.send(toSend);

            socket.receive(received);
            long timeReceived = System.currentTimeMillis();

            long serverTime = Long.valueOf(new String(received.getData()).trim());

            System.out.println("Request Info:");
            System.out.println("Time from client to server: " + (serverTime - timeLeft));
            System.out.println("Time from server to client: " + (timeReceived - serverTime));

            delta = timeReceived - timeLeft;
            System.out.println("Round Trip Time: " + delta);
            System.out.println("#####################");

        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }

        return delta;
    }

    public static void main(String[] args) {

        if (args.length != 3) {
            System.out.println("Usage: <host_ip> <port> <number_of_requests>");
            return;
        }

        try {
            Client client = new Client();
            client.ping(
                    args[0],
                    Integer.valueOf(args[1]),
                    Integer.valueOf(args[2])
            );
        } catch (UnknownHostException e) {
            System.err.println("Unknown Host: " + e.getMessage());

        } catch (SocketException e) {
            System.err.println("Error opening socket: " + e.getMessage());
        }
    }
}
