package org.academiadecodigo.bootcamp.udp_ping.server;

import org.academiadecodigo.bootcamp.udp_ping.Messages;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Server {

    private DatagramSocket socket;

    public Server(int port) throws IOException {
        socket = new DatagramSocket(port);
    }

    public void start() {
        while (!socket.isClosed()) {
            respond();
        }
    }

    private void respond() {
        try {

            byte[] buffer = new byte[512];
            DatagramPacket received = new DatagramPacket(buffer, buffer.length);

            socket.receive(received);
            String message = new String(received.getData()).trim();

            if (!message.equals(Messages.PING)) {
                return;
            }

            buffer = String.valueOf(
                    System.currentTimeMillis()
            ).getBytes();

            DatagramPacket toSend = new DatagramPacket(
                    buffer,
                    buffer.length,
                    received.getAddress(),
                    received.getPort()
            );

            socket.send(toSend);

        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage: <port>");
            return;
        }

        try {
            Server server = new Server(Integer.valueOf(args[0]));
            server.start();

        } catch (NumberFormatException e) {
            System.err.println("Port must be a valid number: " + args[0]);

        } catch (IOException e) {
            System.err.println("Error opening socket: " + e.getMessage());
        }
    }
}
