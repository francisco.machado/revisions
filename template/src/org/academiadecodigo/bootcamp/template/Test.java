package org.academiadecodigo.bootcamp.template;

import java.util.HashMap;
import java.util.Map;

public class Test {

    public static void main(String[] args) {

        Map<String, Object> rodrigo = new HashMap<>();
        rodrigo.put("name", "Rodrigo");
        rodrigo.put("img", "https://scontent.flis5-1.fna.fbcdn.net/v/t31.0-8/22712231_1052803548190055_131029224671721827_o.jpg?_nc_cat=0&oh=4dd918df4dd8f00efb94acf2fd49f3e2&oe=5BB7F869");
        rodrigo.put("description", "Likes long walks on the beach");

        Parser.interpret("resources/template.html", "resources/rodrigo.html", rodrigo);
        Parser.interpret("resources/template.txt", "resources/rodrigo.txt", rodrigo);
    }
}
