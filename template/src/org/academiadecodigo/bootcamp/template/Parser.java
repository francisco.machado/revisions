package org.academiadecodigo.bootcamp.template;

import java.io.*;
import java.util.Map;

public class Parser {

    public static void interpret(String from, String to, Map<String, Object> info) {

        BufferedReader reader = null;
        PrintWriter writer = null;

        try {
            reader = new BufferedReader(new FileReader(from));
            writer = new PrintWriter(new File(to));
            String line;

            while ((line = reader.readLine()) != null) {
                writer.println(parse(line, info));
            }

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            close(reader);
            close(writer);
        }
    }

    private static String parse(String line, Map<String, Object> info) {

        while (line.indexOf('{') != -1) {
            int start = line.indexOf('{');
            int end = line.indexOf('}');

            String key = line.substring(start + 1, end);
            line = line.replace('{' + key + '}', info.get(key).toString());
        }

        return line;
    }

    private static void close(Closeable closeable) {
        if (closeable == null) {
            return;
        }

        try {
            closeable.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
