package org.academiadecodigo.popstars;

import java.io.*;

public class FileHelper {

    public static void print(String filePath) {

        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(filePath));
            String line = reader.readLine();

            while (line != null) {
                System.out.println(line);
                line = reader.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            close(reader);
        }
    }

    public static void copy(String from, String to) {

        BufferedReader reader = null;
        BufferedWriter writer = null;

        try {
            reader = new BufferedReader(new FileReader(from));
            writer = new BufferedWriter(new FileWriter(to));

            String line = reader.readLine();

            while (line != null) {

                line = line.replace("nada", "tudo");

                writer.write(line + "\n");
                line = reader.readLine();
            }

            writer.flush();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close(reader);
            close(writer);
        }

    }

    private static void close(Closeable closeable) {
        if (closeable == null) {
            return;
        }

        try {
            closeable.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}




